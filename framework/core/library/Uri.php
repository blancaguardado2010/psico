<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Uri
{
    # Instancia única de la clase actual.
    private static $instance;

    # Se encarga de verificar si ya existe una instancia de esta clase,
    # si la clase se encuentra instanciada retornará dicha instancia caso
    # contrario creará una nueva instancia antes de retornarla.
    public static function GetInstance()
    {
        if (!isset(self::$instance)) {
            $context = __CLASS__;
            self::$instance = new $context;
        }
        return self::$instance;
    }

    public function segment($position)
    {
        $requestURI = explode('/', $_SERVER['REQUEST_URI']);
        $scriptName = explode('/', $_SERVER['SCRIPT_NAME']);

        for ($i= 0; $i < sizeof($scriptName); $i++) {
            if ($requestURI[$i] == $scriptName[$i]) {
                unset($requestURI[$i]);
            }
        }

        $command = array_values($requestURI);
        $count = count($command);
        if ($count <= 2) {
            return "";
        }
        return isset($command[$position - 1]) ? $command[$position - 1] : "";
    }
}

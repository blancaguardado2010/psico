<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require_once LIBRARY_PATH . "/" . "Loader.php";
require_once LIBRARY_PATH . "/" . "Uri.php";
require_once LIBRARY_PATH . "/" . "Input.php";
require_once LIBRARY_PATH . "/" . "Library.php";

class Controller 
{
	protected $load;
	protected $uri;
	protected $input;
	protected $library;

	public function __construct()
	{
		$this->load = Loader::GetInstance();
		$this->uri = Uri::GetInstance();
		$this->input = Input::GetInstance();
		$this->library = Library::GetInstance();
	}

	public function __get($name)
	{
		if(isset($this->load->models[$name]))
			return $this->load->models[$name];
		else if(isset($this->load->libraries[$name]))
			return $this->load->libraries[$name];
		else if(isset($this->load->helpers[$name]))
			return $this->load->helpers[$name];
	}
}
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

function base_url()
{
	global $config;
	$url = '';
	if(isset($config["base_url"]))
		$url = $config["base_url"];
	return $config["base_url"];
}

function site_url($uri = "")
{
	global $config;
	$url = "";
	if(isset($config["base_url"]))
		$url = $config["base_url"];

	if(isset($config["index_page"]))
		$url .= $config["index_page"] . $uri;
	else
		$url .= $uri;
	return $url;
}

function error_404()
{
	require_once ERRORS_PATH . "/403.html";
}
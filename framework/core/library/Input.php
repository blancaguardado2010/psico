<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Input
{
	# Instancia única de la clase actual.
    private static $instance;

    # Se encarga de verificar si ya existe una instancia de esta clase,
    # si la clase se encuentra instanciada retornará dicha instancia caso
    # contrario creará una nueva instancia antes de retornarla.
    public static function GetInstance()
    {
        if(!isset(self::$instance))
        {
            $context = __CLASS__;
            self::$instance = new $context;
        }
        return self::$instance;
    }
    
	public function post($name)
	{
		return isset($_POST[$name]) ? $_POST[$name] : "";
	}
}
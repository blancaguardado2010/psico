<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Loader
{
    # Contiene todos los modelos llamados por los controladores.
    public $models;
    public $libraries;
    public $helpers;

    # Instancia única de la clase actual.
    private static $instance;

    # Se encarga de verificar si ya existe una instancia de esta clase,
    # si la clase se encuentra instanciada retornará dicha instancia caso
    # contrario creará una nueva instancia antes de retornarla.
    public static function GetInstance()
    {
        if (!isset(self::$instance)) {
            $context = __CLASS__;
            self::$instance = new $context;
        }
        return self::$instance;
    }

    # Se declara como privado el constructor de la clase para
    # que no pueda ser instanciada desde otro lugar.
    private function __construct()
    {
    }

    # No se permite que la clase sea clonada para respetar
    # el patron Singleton y que exista una instancia única
    # de esta clase.
    public function __clone()
    {
        trigger_error('La clonación de este objeto no está permitida', E_USER_ERROR);
    }

    # Este método se encarga de cargar las vistas solicitadas por el controlador
    # asi como cargar variables que se envien a esta vista.
    public function view($contentFile, $data = [])
    {
        $contentFileFullPath = VIEWS_PATH . "/" . $contentFile . ".php";
        if (!file_exists($contentFileFullPath)) {
            error_404();
            return;
        }
        # Se asegura que cada variable dentro del array data sea
        # accesible para las vistas.
        # Cada llave en el array $data se convertirá en una variable.
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                if (strlen($key) > 0) {
                    ${$key} = $value;
                }
            }
        }

        require_once($contentFileFullPath);
    }

    # Carga las vistas que funcionan como plantillas para la aplicación.
    public function template($contentFile, $data = [])
    {
        $contentFileFullPath = TEMPLATES_PATH . "/" . $contentFile . ".php";
        if (!file_exists($contentFileFullPath)) {
            error_404();
            return;
        }

        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                if (strlen($key) > 0) {
                    ${$key} = $value;
                }
            }
        }

        require_once($contentFileFullPath);
    }

    # Carga los modelos solicitados por el controlador y si se ha
    # especificado un nombre diferente, se le asigna para que pueda ser
    # llamado por dicho nombre.
    public function model($model_name, $rename = "", $mod = "")
    {
        $model_name = strtolower($model_name);
        $rename = $rename;

        $contentFileFullPath = MODELS_PATH . (($mod == "")? '' : '/'.$mod). "/" . $model_name . ".php";
        if (!file_exists($contentFileFullPath)) {
            error_404();
            return;
        }
        $arrNames = explode("_", $model_name);
        require_once $contentFileFullPath;
        $className = ucfirst($arrNames[0]) . "Model";
        $instance = new $className;
        $objectName = $rename == "" ? $model_name : $rename;
        $this->models[$objectName] = $instance;
    }


    public function library($library_name, $config = [], $data = [])
    {
        $library_name = strtolower($library_name);
        $contentFileFullPath = BASEPATH . "/app/libraries/" . $library_name . ".php";
        if (!file_exists($contentFileFullPath)) {
            error_404();
            return;
        }

        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                if (strlen($key) > 0) {
                    ${$key} = $value;
                }
            }
        }
        require_once $contentFileFullPath;
        $className = ucfirst($library_name);
        $instance = new $className;
        $this->libraries[$library_name] = $instance;
    }

    public function helper($helper_name, $helper_rename = "", $data = [])
    {
        $helper_name = strtolower($helper_name);
        $contentFileFullPath = BASEPATH . "/app/helpers/" . $helper_name . "_helper.php";
        if (!file_exists($contentFileFullPath)) {
            error_404();
            return;
        }

        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                if (strlen($key) > 0) {
                    ${$key} = $value;
                }
            }
        }
        require_once $contentFileFullPath;
        $className = ucfirst($helper_name) . "Helper";
        $instance = new $className;
        $this->helpers[$helper_rename == "" ? $helper_name : $helper_rename] = $instance;
    }
}

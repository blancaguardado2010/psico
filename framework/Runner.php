<?php 

defined("BASEPATH")
    or define("BASEPATH", realpath(dirname(__FILE__) . '/../'));

require_once BASEPATH . '/app/config/Config.php';
require_once BASEPATH . '/app/config/Routes.php';


defined("LIBRARY_PATH")
    or define("LIBRARY_PATH", realpath(dirname(__FILE__) . '/core/library'));
     
defined("TEMPLATES_PATH")
    or define("TEMPLATES_PATH", BASEPATH . '/app/views/templates');
     
defined("ERRORS_PATH")
    or define("ERRORS_PATH", realpath(dirname(__FILE__) . '/core/templates/errors'));

defined("CONTROLLERS_PATH")
    or define("CONTROLLERS_PATH", BASEPATH . '/app/controllers');

defined("VIEWS_PATH")
    or define("VIEWS_PATH", BASEPATH . '/app/views');

defined("MODELS_PATH")
    or define("MODELS_PATH", BASEPATH . '/app/models');

defined("ASSETS_PATH")
    or define("ASSETS_PATH", BASEPATH . '/assets');

 
/*
    Error reporting.
*/
ini_set("error_reporting", "true");
error_reporting(E_ALL|E_STRCT);

require_once LIBRARY_PATH . "/Common.php";
require_once LIBRARY_PATH . "/Controller.php";
require_once LIBRARY_PATH . "/Model.php";
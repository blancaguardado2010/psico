function eliminar(datos) {
	$modal = $('#modal_global');
	swal
	({
	  title: '¿Seguro que desea eliminar este registro?',
	  text: "¡No podras revertir esto!",
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  cancelButtonText: 'Cancelar',
	  confirmButtonText: 'Si, ¡Deseo Borrarlo!'
	}).then((result) => {
		if (result.value)
		{
			$.ajax({
				type: 'get',
				dataType: 'json',
				url: $modal.attr('data-target').replace("modal", "remove/") + datos
			})
			.done(function(data) {
				console.log("success");
				reloadData();
				mostrar_alert(data);
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		}
	});
};
function add_html(input,html,funt="modal",mod=0,scroll=$('.modal-body')) {
	datos = parseInt(input.attr('contador'));
	dato = parseInt(input.attr('dato'));
	$params = datos+1;
	$modal = $('#modal_global');
	$.ajax({
		type: 'get',
		dataType: 'html',
		url: $modal.attr('data-target').replace('modal',funt) + '/' + $params + '/' + dato
	})
	.done(function(response) {
		input.attr('contador', $params);
		altura = html.height();
		scroll.animate({scrollTop:altura+"px"});
		if (mod) {

		} else {
			$('.contador-pre').hide();
			$(".cont-"+($params-1)).addClass("col-md-12");
			$(".cont-"+($params-1)).removeClass("col-md-10");
			$(".bottum-"+($params-1)).hide();
		}	
		html.append(response);
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});

};
function del_html(input,contador,mod=0) {
	input.remove();
	datos = parseInt(contador.attr('contador'));
	$params = datos-1;
	contador.attr('contador', $params);
	if (mod) {

	} else {
		$('.contador-pre').show();
		$(".cont-"+($params)).addClass("col-md-10");
		$(".cont-"+($params)).removeClass("col-md-12");
		$(".bottum-"+($params)).show();	
	}
}
function sheck(contador,mod=0,funt='session_pregunta') {
	$modal = $('#modal_secundario');
	if (mod) {

	} else {
		Pregunta = $('.Pregunta-'+contador).val();
		Id_tipo_test = $('.Id_tipo_test-'+contador).val();
		if (Pregunta == "" || Id_tipo_test == "") {
			mostrar_alert("Error,Los campos son obligatorios!,danger,top,center");
			return false;
		}
		$.ajax({
					url: $modal.attr('data-target').replace('modal',funt),
					type: 'POST',
					//dataType: 'json',
					data: {Pregunta: Pregunta,Id_tipo_test: Id_tipo_test,contador: contador},
				})
				.done(function() {
					console.log("success");
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
						
	}
}
var valida = 0;
function modals(datos,funt="modal",dato="",val=0,secun=0) {
	$params = datos;
	$modal ="";
	if (dato != "0" && val==1) {
		return false;
	}
	if (secun) {
		$modal = $('#modal_secundario');
		valida = 1;
		$('#page-top').css("overflow-y", "scroll");
    	$('#modal_global').modal('hide');
	}else{
		$modal = $('#modal_global');
	}
	$modal.html('');
	$.ajax({
		type: 'get',
		dataType: 'html',
		url: $modal.attr('data-target').replace('modal',funt) + '/'+ $params
	})
	.done(function(response) {
		$modal.html(response);
		$modal.modal('show');
		console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
};
$("#modal_secundario").on('hidden.bs.modal', function() {
    valida = 0;
    $('#modal_global').modal('show');
    $('#page-top').css("overflow-y", "");
});
$('.save').click(function(event) {
	console.log('entro');
	event.preventDefault();
	$('.add').submit();
});
$('.add').submit(function(event) {
	//console.log('entro');
	event.preventDefault();
	var $form = $(this);
	if (validacion_campos()) {
		datos = "Error,Todos los campos son obligatorios,danger,top,center";
  		mostrar_alert(datos);
		return false;
	}
	if (validacion_tamano_numeros()) {
		return false;
	}
	//console.log($form.attr("data-target"));
	var $formData = new FormData(this);
	$.ajax({
		method: 'post',
		dataType: 'json',
		url: $form.attr("data-target"),
		data: $formData,
	    contentType:false,
        cache:false,
        processData:false,
	})
	.done(function(data) {
		console.log("success");
		mostrar_alert(data);
		reloadData();
		$('#modal_global').modal('toggle');

	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});

});
function mostrar_alert(datos) {
	var $datos = datos.split([',']);
	$.notify({
		title: '<strong>'+$datos[0]+'!</strong>',
		message: $datos[1]+'!'
	},{
		type: $datos[2],
		z_index: '10000',
		placement: {
	  		from: $datos[3],
	  		align: $datos[4]
		}
	});
}
function mostrar_alert_sw(datos) {
  var $datos = datos.split([',']);
  swal({
    position: $datos[0],
    type: $datos[1],
    title: $datos[2],
    showConfirmButton: false,
    timer: $datos[3]
  });
}
function validacion_campos() {
	var $error = false;
	$('.add input, .add select').each(function(index, el) {
		var $input = $(this);
		if ($input.attr('required')) {
			if ($input.val() === '' || $input.val() <= 0) {
				$ms = $(this).attr('data-target-ms');
				$datos = "Error,"+$ms+",danger,top,center";
      			/*mostrar_alert(datos);*/
      			$error = true;
      			$input.addClass('invalid');
      			if ($input.siblings('label.label').length == 0) {
      				$input.after('<label class="invalid label">'+$ms+'</label>');
      			}
			}else {
				$input.removeClass('invalid');
				$input.siblings('label.label').remove();
			}
		}
	});
	return $error;
}
function validacion_tamano_numeros() {
	var $error = false;
	$('.add input, .add select').each(function(index, el) {
		var $input = $(this);
		if ($input.attr('required')) {
			if (String($input.val()).length < $input.attr('tamano') && $input.is('[tamano]')) {
				$ms = $(this).attr('data-target-ms-numero');
				$datos = "Error,"+$ms+",danger,top,center";
      			/*mostrar_alert(datos);*/
      			$error = true;
      			$input.addClass('invalid');
      			if ($input.siblings('label.label').length == 0) {
      				$input.after('<label class="invalid label">'+$ms+'</label>');
      			}
			}else {
				$input.removeClass('invalid');
				$input.siblings('label.label').remove();
			}
		}
	});
	return $error;
}
//validaciones generales
$('.numero').keypress(function(e) {
  tecla = (document.all) ? e.keyCode : e.which;

  //Tecla de retroceso para borrar, siempre la permite
  if (tecla == 8 || tecla == 0) {
    return true;
  }

  // Patron de entrada, en este caso solo acepta numeros
  patron = /[0-9-.]/;
  tecla_final = String.fromCharCode(tecla);
  return patron.test(tecla_final);
});
$(".Telefono").mask("(000) 0000-0000");
$(".Dui").mask("00000000-0");
//fin validaciones generales
$('.selectpicker').selectpicker().val();//selectpicker


/* Esta función se utiliza despues de registrar
 * se encarga de obtener los registros del base de datos y
 * actualiza el datasource del grid.
 */
function reloadData()
{
	/* ".basic-info-form" contiene la ruta de donde se obtendra la información.  */
	var form = $("#modal_global");

	/* Atravéz de ajax se obtienen los resultados sin tener que recargar la página */
	$.ajax({
	    dataType: 'json',
	    url: form.attr('data-target').replace('modal','all'),
	    success:function(response){
	    	var grid = $('#grid').getKendoGrid();
	    	grid.dataSource.data(response);
	        grid.refresh();
	    }
	});
}


/* Al cargar la página se procede a cargar la información de los empleados */
$.ajax({  
    type: 'get',
    dataType: 'json', 
    url: $("#grid").attr('reload-data-target'),
    success:function(response){
        /* se le asigna la información al grid */
        var grid = $('#grid').getKendoGrid();
        grid.dataSource.data(response);
        grid.refresh();
    }
});

/* Se inicializa el grid y las columnas y formatos que contendrá
 * Ejemplo de uso:
 * https://demos.telerik.com/kendo-ui/grid/index
 */
$("#grid").kendoGrid({
    toolbar: [ 
        { template: kendo.template($("#template").html()) }
    ],
    dataSource: {
        type: "odata",
        transport: {
        },
        pageSize: 20,
        schema: {
            model: {
                id: 'Id_Alumno',
                fields: {
                    Id_Alumno : {
                        type: 'number',
                        editable: false,
                        nullable: false
                    },
                    Religion : {
                        type: 'string',
                        editable: false,
                        nullable: false
                    },
                    FechaNacimiento : {
                        type: 'string',
                        editable: false,
                        nullable: false
                    },

                    Direccion : {
                        type: 'string',
                        editable: false,
                        nullable: false
                    },
                    Telefono : {
                        type: 'string',
                        editable: false,
                        nullable: false
                    },
                    Vive_Con : {
                        type: 'number',
                        editable: false,
                        nullable: false
                    },
                    Nombre : {
                        type: 'string',
                        editable: false,
                        nullable: false
                    }
                }
            }
        }
    },
    height: 700,
    groupable: true,
    sortable: true,
    pageable: {
        refresh: true,
        pageSizes: true,
        buttonCount: 5
    },
    columns: [        
    {
        field: "Nombre",
        title: "Nombre",
        width: 100,
        headerAttributes: { style: "color: #646464;" },
        attributes: { style: "text-align: center"}
    },
     {
        field: "Vive_Con",
        title: "vive con",
        width: 100,
        headerAttributes: { style: "color: #646464;" },
        attributes: { style: "text-align: center"}
    },
	{
        field: "Religion",
        title: "Religion",
        width: 100,
        headerAttributes: { style: "color: #646464;" },
        attributes: { style: "text-align: center"}
    },
    {
        field: "FechaNacimiento",
        title: "Fecha Nacimiento",
        width: 100,
        headerAttributes: { style: "color: #646464;" },
        attributes: { style: "text-align: center"}
    },
    {
        field: "Direccion",
        title: "Direccion",
        width: 100,
        headerAttributes: { style: "color: #646464;" },
        attributes: { style: "text-align: center"}
    },
    {
        field: "Telefono",
        title: "Telefono",
        width: 100,
        headerAttributes: { style: "color: #646464;" },
        attributes: { style: "text-align: center"}
    },
  
	
    {
      title: "Opciones",
      template: "<a class='btn btn-primary' onclick='modals(#= Id_Alumno  #)'  data-toggle='tooltip' data-placement='left' style='color: white' > <i class='far fa-edit'></i></a>&nbsp;&nbsp;&nbsp;<a class='btn btn-danger eliminar' onclick='eliminar(#= Id_Alumno  #)'  data-toggle='tooltip' data-placement='left' style='color: white' > <i class='fas fa-trash-alt'></i></a>",
      width: 64
    }],
    selectable: "row",
    filterable: true,
});
/*
 * 
 * Cuando se escribe dentro de la caja de búsqueda se captura el evento y lo
 * que el usuario ha escrito para realizar la busqueda dentro del grid.
 *
 *
 */
$("input.search-buscar").on('input', function()
{
    /* Se captura lo que el usuario ha escrito */
    var q = $(this).val();
    /* Obtiene el grid que contiene la información */
    var grid = $("#grid").data("kendoGrid");
    /* Se realiza la búsqueda dentro del grid 
     * Ejemplo de uso:
     * https://telerikhelper.net/2014/10/21/how-to-external-search-box-for-kendo-ui-grid/
     */
    grid.dataSource.query({
        page:1,
        pageSize:20,
        filter:{
            logic:"or", // "or" o "and"
            filters:[               
                {field:"Nombre", operator:"contains", value:q}, // se puede usar operator: 'eq', si se desea que la columna contenga exactamente el mismo valor que el usuario ingreso
                {field:"Telefono", operator:"contains", value:q},
                {field:"Religion", operator:"contains", value:q}
            ]
        }
    }); 
});

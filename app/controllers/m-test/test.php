<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TestController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('test_model', 'model', 'm-test');
        $this->load->model('preguntastest_model', 'preguntas', 'm-test');
        $this->load->model('tipotest_model', 'tipo', 'm-test');
        $this->load->library('menu');
        $this->load->helper('menu', 'menu_helper');
    }

    public function index()
    {
        $data["js"] = array(
            $this->library->modulosjs("loader","m-test","test"),
            $this->library->modulosjs("main"));
        $data['css'] = array(
            $this->library->vendorcss("kendo.bootstrap-v4.min","kendo","css")
        );
        $data['tabla'] = "m-test/test";
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $data['title'] = "test";
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('views', $data);
        $this->load->view('templates/footer', $data);
    }
    public function all()
    {
        $data = $this->model->get_consulta();
        echo json_encode($data);
    }
    public function modal()
    {
        $Id = $this->uri->segment(4);
        $data['tipotest'] = $this->tipo->get_consulta();
        if ($Id > 0 ) {
            $data['data'] = $this->model->get_id($Id);
            $this->load->view("modals/m-test/test",$data);
        }else{
            $this->load->view("modals/m-test/test",$data);
        }
    }
    public function preguntas()
    {
        $Id = $this->uri->segment(4);
        $data['preguntas'] = $this->preguntas->get_id_test($Id);
        $data['id'] = $Id;
        $this->load->view("modals/m-test/preguntas",$data);       
    }
    public function add_preguntas()
    {
        $pos = $this->uri->segment(4);
        $data['pos'] = $pos;
        $this->load->view("modals/m-test/complementos-preguntas/add_pregunta",$data);
    }
    public function session_pregunta()
    {
        if (!isset($_SESSION['preguntas'])) {
            $_SESSION['preguntas'] = array(
                array(
                    "operacion" => 1, //false
                    "pregunta" => trim($this->input->post("Pregunta")),//$pregunta
                    "respuestas" => array(),
                )
            );
        }else{
            $pregun = array(
                "operacion" => 1, //false
                "pregunta" => trim($this->input->post("Pregunta")),//$pregunta
                "respuestas" => array(),
            );
            array_push($_SESSION['preguntas'],$pregun);
        }
        if (!trim($this->input->post("Id_tipo_test"))) {
            self::respuestas(trim($this->input->post("contador")));
        }
        
    }
    public function add_respuestas()
    {
        $pos = $this->uri->segment(4);
        $data['pos'] = $pos;
        $this->load->view("modals/m-test/complementos-preguntas/add_respuestas",$data);
    }
    public function respuestas()
    {
        $numero = $this->uri->segment(4);
        $data['data'] = $this->model->get_id($numero);
        $this->load->view("modals/m-test/respuestas",$data);
    }
    public function save()
    {
        $info = array(
            ':Abreviatura' => strtoupper(trim($this->input->post("Abreviatura"))),
            ':Test' => strtoupper(trim($this->input->post("Test"))),
            ':Id_tipo_test' => strtoupper(trim($this->input->post("Id_tipo_test")))
        );
        $save = $this->model->save($info);
        if ($save != 0) {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }else{
            $datos = "success,Se agrego con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    public function update()
    {
        $info = array(
            ':Test' => strtoupper(trim($this->input->post("Test"))),
            ':Id_tipo_test' => strtoupper(trim($this->input->post("Id_tipo_test"))),
            ':Id_test' => strtoupper(trim($this->input->post("Id")))
        );
        $save = $this->model->update($info);
        if ($save != 0) {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }else{
            $datos = "success,Se edito un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }

    public function remove()
    {
        $info = array(
            ':Eliminado' => strtoupper(trim(1)),
            ':Id_test' => strtoupper(trim($this->uri->segment(4)))
        );
        $save = $this->model->remove($info);
        if ($save != 0) {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }else{
            $datos = "success,Se elimino un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
}
?>

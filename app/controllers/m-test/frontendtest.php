<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FrontendtestController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('test_model', 'model', 'm-test');
        $this->load->model('tipotest_model', 'tipo', 'm-test');
        $this->load->library('menu');
        $this->load->helper('menu', 'menu_helper');
    }

    public function index()
    {
        $data["js"] = array(
            $this->library->modulosjs("loader","m-test","test"),
            $this->library->modulosjs("main"));
        $data['css'] = array(
            $this->library->vendorcss("kendo.bootstrap-v4.min","kendo","css")
        );
        $data['tabla'] = "m-test/test";
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $data['title'] = "test";
        $this->load->view('templates_test/header', $data);
        $this->load->view('templates_test/sidebars', $data);
        $this->load->view('realizar_test', $data);
        $this->load->view('templates_test/footer', $data);
    }
    public function all()
    {
        $data = $this->model->get_consulta();
        echo json_encode($data);
    }
    public function modal()
    {
        $Id = $this->uri->segment(4);
        $data['tipotest'] = $this->tipo->get_consulta();
        if ($Id > 0 ) {
            $data['data'] = $this->model->get_id($Id);
            $this->load->view("modals/m-test/test",$data);
        }else{
            $this->load->view("modals/m-test/test",$data);
        }
    }
    public function save()
    {
        $info = array(
            ':Test' => strtoupper(trim($this->input->post("Test"))),
            ':Id_tipo_test' => strtoupper(trim($this->input->post("Id_tipo_test")))
        );
        $save = $this->model->save($info);
        if ($save != 0) {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }else{
            $datos = "success,Se agrego con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    public function update()
    {
        $info = array(
            ':Test' => strtoupper(trim($this->input->post("Test"))),
            ':Id_tipo_test' => strtoupper(trim($this->input->post("Id_tipo_test"))),
            ':Id_test' => strtoupper(trim($this->input->post("Id")))
        );
        $save = $this->model->update($info);
        if ($save != 0) {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }else{
            $datos = "success,Se edito un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }

    public function remove()
    {
        $info = array(
            ':Eliminado' => strtoupper(trim(1)),
            ':Id_test' => strtoupper(trim($this->uri->segment(4)))
        );
        $save = $this->model->remove($info);
        if ($save != 0) {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }else{
            $datos = "success,Se elimino un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
}
?>

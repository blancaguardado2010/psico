<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TipotestController extends Controller 
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('tipotest_model', 'model', 'm-test');
        $this->load->library('menu');
        $this->load->helper('menu', 'menu_helper');
    }

    public function index()
    {
        $data["js"] = array(
            $this->library->modulosjs("loader","m-test","tipotest"),
            $this->library->modulosjs("main"));
        $data['css'] = array(   
            $this->library->vendorcss("kendo.bootstrap-v4.min","kendo","css")
        );
        $data['tabla'] = "m-test/tipotest";
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $data['title'] = "tipos_test";
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('views', $data);
        $this->load->view('templates/footer', $data);
    }
    public function all()
    {
        $data = $this->model->get_consulta();
        echo json_encode($data);
    }
    public function modal()
    {
        $Id = $this->uri->segment(4);
        if ($Id > 0 ) {
            $data['data'] = $this->model->get_id($Id);
            $this->load->view("modals/m-test/tipotest",$data);
        }else{
            $this->load->view("modals/m-test/tipotest");
        }
    }
    public function save()
    {
        $info = array(
            ':Tipos_test' => strtoupper(trim($this->input->post("Tipos_test")))
        );
        $save = $this->model->save($info);
        if ($save != 0) {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }else{
            $datos = "success,Se agrego con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    public function update()
    {
        $info = array(
            ':Tipos_test' => strtoupper(trim($this->input->post("Tipos_test"))),
            ':Id_tipo_test' => strtoupper(trim($this->input->post("Id")))
        );
        $save = $this->model->update($info);
        if ($save != 0) {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }else{
            $datos = "success,Se edito un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    
    public function remove()
    {
        $info = array(
            ':Eliminado' => 1,
            ':Id_tipo_test' => strtoupper(trim($this->uri->segment(4)))
        );
        $save = $this->model->remove($info);
        if ($save != 0) {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }else{
            $datos = "success,Se elimino un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
}
?>
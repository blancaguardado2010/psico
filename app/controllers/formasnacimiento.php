<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FormasnacimientoController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('formasnacimiento_model', 'formas_nacimiento');
        $this->load->library('menu');
        $this->load->helper('menu', 'menu_helper');
    }

    public function index()
    {
        $data["js"] = array(
            $this->library->modulosjs("loader","formas_nacimiento"),
            $this->library->modulosjs("main"));
        $data['css'] = array(
            $this->library->vendorcss("kendo.bootstrap-v4.min","kendo","css")
        );
        $data['tabla'] = "formasnacimiento";
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $data['title'] = "Modulo de formas_nacimiento";
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('views', $data);
        $this->load->view('templates/footer', $data);
    }
    public function all()
    {
        $data = $this->formas_nacimiento->get_consulta();
        echo json_encode($data);
    }
    public function modal()
    {
        $Id = $this->uri->segment(3);

        if ($Id > 0 ) {
            $data['data'] = $this->formas_nacimiento->get_id($Id);
            $this->load->view("modals/formas_nacimiento",$data);
        }else{
            $this->load->view("modals/formas_nacimiento");
        }
    }
    public function save()
    {
        $info = array(
            ':Nombre' => trim($this->input->post("Nombre"))
        );
        $save = $this->formas_nacimiento->save($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se agrego con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    public function update()
    {
        $info = array(
            ':Nombre' => trim($this->input->post("Nombre")),
            ':Id_Forma_Nacimiento' => strtoupper(trim($this->input->post("Id")))
        );
        $save = $this->formas_nacimiento->update($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se edito un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    public function remove()
    {
        $info = array(
            ':Id_Forma_Nacimiento' => strtoupper(trim($this->uri->segment(3))),
            ':Eliminado' => 1
        );
        $save = $this->formas_nacimiento->remove($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se elimino un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }

}
?>

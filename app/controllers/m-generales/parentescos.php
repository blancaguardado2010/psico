<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ParentescosController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('parentescos_model', 'parentescos', 'm-generales');
        $this->load->library('menu');
        $this->load->helper('menu', 'menu_helper');
    }

    public function index()
    {
        $data["js"] = array(
            $this->library->modulosjs("loader","m-generales","parentescos"),
            $this->library->modulosjs("main"));
        $data['css'] = array(
            $this->library->vendorcss("kendo.bootstrap-v4.min","kendo","css")
        );
        $data['tabla'] = "m-generales/parentescos";
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $data['title'] = "Modulo de parentescos";
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('views', $data);
        $this->load->view('templates/footer', $data);
    }
    public function all()
    {
        $data = $this->parentescos->get_consulta();
        echo json_encode($data);
    }
    public function modal()
    {
        $Id = $this->uri->segment(4);

        if ($Id > 0 ) {
            $data['data'] = $this->parentescos->get_id($Id);
            $this->load->view("modals/m-generales/parentescos",$data);
        }else{
            $this->load->view("modals/m-generales/parentescos");
        }
    }
    public function save()
    {
        $info = array(
            ':Nombre' => trim($this->input->post("Nombre"))
        );
        $save = $this->parentescos->save($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se agrego con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    public function update()
    {
        $info = array(
            ':Nombre' => trim($this->input->post("Nombre")),
            ':Id_Parentesco' => strtoupper(trim($this->input->post("Id")))
        );
        $save = $this->parentescos->update($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se edito un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    public function remove()
    {
        $info = array(
            ':Id_Parentesco' => trim($this->uri->segment(4)),
            ':Eliminado' => 1
        );
        $save = $this->parentescos->remove($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se elimino un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }

}
?>

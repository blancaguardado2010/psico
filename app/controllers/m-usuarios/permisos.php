
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PermisosController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('permisos_model', 'permisos', 'm-usuarios');

        $this->load->library('menu');
        $this->load->helper('menu', 'menu_helper');
    }

    public function index()
    {
        $data["js"] = array(
            $this->library->modulosjs("loader","m-usuarios","permisos"),
            $this->library->modulosjs("main"));
        $data['css'] = array(
            $this->library->vendorcss("kendo.bootstrap-v4.min","kendo","css")
        );
        $data['tabla'] = "m-usuarios/permisos";
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $data['title'] = "Modulo de permisos";
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('views', $data);
        $this->load->view('templates/footer', $data);
    }
    public function all()
    {
        $data = $this->permisos->get_consulta();
        echo json_encode($data);
    }
    public function modal()
    {
        $Id = $this->uri->segment(4);

        if ($Id > 0 ) {
            $data['data'] = $this->permisos->get_id($Id);
            $this->load->view("modals/m-usuarios/permisos",$data);
        }else{
            $this->load->view("modals/m-usuarios/permisos");
        }
    }
    public function save()
    {
        $info = array(

            ':Permiso' => trim($this->input->post("Permiso")),
            ':Nombre' => trim($this->input->post("Nombre"))
        );
        $save = $this->permisos->save($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se agrego con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    public function update()
    {
        $info = array(

            ':Nombre' => trim($this->input->post("Nombre")),
            ':Id_Permisos' => strtoupper(trim($this->input->post("Id")))
        );
        $save = $this->permisos->update($info);
        if ($this->input->post("Nombre") != '') {
            $info1 = array(
                ':Permiso' => trim($this->input->post("Permiso")),
                ':Nombre' => trim($this->input->post("Nombre")),
                ':Id_Permisos' => strtoupper(trim($this->input->post("Id")))
            );
            $save1 = $this->permisos->update_Nombre($info1);
        }
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se edito un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    public function remove()
    {
        $info = array(
            ':Id_Permisos' => trim($this->uri->segment(4)),
            ':Eliminado' => 1
        );
        $save = $this->permisos->remove($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se elimino un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }

}
?>

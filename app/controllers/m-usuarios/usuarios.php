<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsuariosController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('usuarios_model', 'usurios', 'm-usuarios');
        $this->load->model('roles_model', 'roles', 'm-usuarios');
        $this->load->model('estados_model', 'estados');
        $this->load->library('menu');
        $this->load->helper('menu', 'menu_helper');
    }

    public function index()
    {
        $data["js"] = array(
            $this->library->modulosjs("loader","m-usuarios","usuarios"),
            $this->library->modulosjs("main"));
        $data['css'] = array(
            $this->library->vendorcss("kendo.bootstrap-v4.min","kendo","css")
        );
        $data['tabla'] = "m-usuarios/usuarios";
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $data['title'] = "Modulo de usuarios";
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('views', $data);
        $this->load->view('templates/footer', $data);
    }
    public function all()
    {
        $data = $this->usurios->get_consulta();
        echo json_encode($data);
    }
    public function modal()
    {
        $Id = $this->uri->segment(4);
        $data['roles'] = $this->roles->get_consulta();
        $data['estados'] = $this->estados->get_consulta();
        if ($Id > 0 ) {
            $data['data'] = $this->usurios->get_id($Id);
            $this->load->view("modals/m-usuarios/usuarios",$data);
        }else{
            $this->load->view("modals/m-usuarios/usuarios",$data);
        }
    }
    public function save()
    {
        $info = array(
            ':Id_Rol' => trim($this->input->post("Id_Rol")),
            ':Id_Estado' => trim($this->input->post("Id_Estado")),
            ':User' => trim($this->input->post("User")),
            ':Password' => md5($this->input->post("Password"))
        );
        $save = $this->usurios->save($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se agrego con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    public function update()
    {
        $info = array(
            ':Id_Rol' => trim($this->input->post("Id_Rol")),
            ':Id_Estado' => trim($this->input->post("Id_Estado")),
            ':User' => trim($this->input->post("User")),
            ':Id_Usuario' => strtoupper(trim($this->input->post("Id")))
        );
        $save = $this->usurios->update($info);
        if ($this->input->post("Password") != '') {
            $info1 = array(
                ':Password' => md5($this->input->post("Password")),
                ':Id_Usuario' => strtoupper(trim($this->input->post("Id")))
            );
            $save1 = $this->usurios->update_password($info1);
        }
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se edito un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    public function remove()
    {
        $info = array(
            ':Id_Usuario' => trim($this->uri->segment(4)),
            ':Eliminado' => 1
        );
        $save = $this->usurios->remove($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se elimino un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }

}
?>

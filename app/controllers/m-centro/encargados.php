<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EncargadosController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('encargados_model', 'encargados', 'm-centro');

        $this->load->library('menu');
        $this->load->helper('menu', 'menu_helper');
    }

    public function index()
    {
        $data["js"] = array(
            $this->library->modulosjs("loader","m-centro","encargados"),
            $this->library->modulosjs("main"));
        $data['css'] = array(
            $this->library->vendorcss("kendo.bootstrap-v4.min","kendo","css")
        );
        $data['tabla'] = "m-centro/encargados";
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $data['title'] = "Modulo de encargados";
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('views', $data);
        $this->load->view('templates/footer', $data);
    }
    public function all()
    {
        $data = $this->encargados->get_consulta();
        echo json_encode($data);
    }
    public function modal()
    {
        $Id = $this->uri->segment(4);

        if ($Id > 0 ) {
            $data['data'] = $this->encargados->get_id($Id);
            $this->load->view("modals/m-centro/encargados",$data);
        }else{
            $this->load->view("modals/m-centro/encargados");
        }
    }
    public function save()
    {
        $info = array(
            ':Correo' => trim($this->input->post("Correo")),
            ':Telefono' => trim($this->input->post("Telefono")),
            ':Dui' => trim($this->input->post("Dui")),
            ':Nombre' => trim($this->input->post("Nombre"))
        );
        $save = $this->encargados->save($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se agrego con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    public function update()
    {
        $info = array(
            ':Correo' => trim($this->input->post("Correo")),
            ':Telefono' => trim($this->input->post("Telefono")),
            ':Dui' => trim($this->input->post("Dui")),
            ':Nombre' => trim($this->input->post("Nombre")),
            ':Id_Encargado' => strtoupper(trim($this->input->post("Id")))
        );
        $save = $this->encargados->update($info);
        
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se edito un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    public function remove()
    {
        $info = array(
            ':Id_Encargado' => trim($this->uri->segment(4)),
            ':Eliminado' => 1
        );
        $save = $this->encargados->remove($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se elimino un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }

}
?>

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DocentesController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('docentes_model', 'docentes', 'm-centro');

        $this->load->library('menu');
        $this->load->helper('menu', 'menu_helper');
    }

    public function index()
    {
        $data["js"] = array(
            $this->library->modulosjs("loader","m-centro","docentes"),
            $this->library->modulosjs("main"));
        $data['css'] = array(
            $this->library->vendorcss("kendo.bootstrap-v4.min","kendo","css")
        );
        $data['tabla'] = "m-centro/docentes";
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $data['title'] = "Modulo de docentes";
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('views', $data);
        $this->load->view('templates/footer', $data);
    }
    public function all()
    {
        $data = $this->docentes->get_consulta();
        echo json_encode($data);
    }
    public function modal()
    {
        $Id = $this->uri->segment(4);

        if ($Id > 0 ) {
            $data['data'] = $this->docentes->get_id($Id);
            $this->load->view("modals/m-centro/docentes",$data);
        }else{
            $this->load->view("modals/m-centro/docentes");
        }
    }
    public function save()
    {
        $info = array(
            ':Direccion' => trim($this->input->post("Direccion")),
            ':Telefono' => trim($this->input->post("Telefono")),

            ':Nombre' => trim($this->input->post("Nombre"))
        );
        $save = $this->docentes->save($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se agrego con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    public function update()
    {
        $info = array(
            ':Direccion' => trim($this->input->post("Direccion")),
            ':Telefono' => trim($this->input->post("Telefono")),
            ':Nombre' => trim($this->input->post("Nombre")),
            ':Id_docente' => strtoupper(trim($this->input->post("Id")))
        );
        $save = $this->docentes->update($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se edito un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    public function remove()
    {
        $info = array(
            ':Id_docente' => trim($this->uri->segment(4)),
            ':Eliminado' => 1
        );
        $save = $this->docentes->remove($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se elimino un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }

}
?>

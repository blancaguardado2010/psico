<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CentrosescolaresController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('centrosescolares_model', 'centrosescolares', 'm-centro');
        $this->load->model('encargados_model', 'encargados', 'm-centro');
        $this->load->model('municipios_model', 'municipios', 'm-generales');
        $this->load->library('menu');
        $this->load->helper('menu', 'menu_helper');
    }

    public function index()
    {
        $data["js"] = array(
            $this->library->modulosjs("loader","m-centro","centrosescolares"),
            $this->library->modulosjs("main"));
        $data['css'] = array(
            $this->library->vendorcss("kendo.bootstrap-v4.min","kendo","css")
        );
        $data['tabla'] = "m-centro/centrosescolares";
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('views', $data);
        $this->load->view('templates/footer', $data);
    }
    public function all()
    {
        $data = $this->centrosescolares->get_consulta();
        echo json_encode($data);
    }
    public function modal()
    {
        $Id = $this->uri->segment(4);
        $data['encargados'] = $this->encargados->get_consulta();
        $data['municipios'] = $this->municipios->get_consulta();
        if ($Id > 0 ) {
            $data['data'] = $this->centrosescolares->get_id($Id);
            $this->load->view("modals/m-centro/centrosescolares",$data);
        }else{
            $this->load->view("modals/m-centro/centrosescolares",$data);
        }
    }
    public function save()
    {
        $info = array(
            ':Id_Encargado' => trim($this->input->post("Id_Encargado")),
            ':Id_Municipio' => trim($this->input->post("Id_Municipio")),
            ':Nombre' => trim($this->input->post("Nombre")),
            ':Direccion' => trim($this->input->post("Direccion")),
            ':Telefono' => trim($this->input->post("Telefono")),
            ':Codigo' => trim($this->input->post("Codigo")),



        );
        $save = $this->centrosescolares->save($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se agrego con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
     public function update()
    {
        $info = array(
           ':Id_Encargado' => trim($this->input->post("Id_Encargado")),
            ':Id_Municipio' => trim($this->input->post("Id_Municipio")),
            ':Nombre' => trim($this->input->post("Nombre")),
            ':Direccion' => trim($this->input->post("Direccion")),
            ':Telefono' => trim($this->input->post("Telefono")),
            ':Codigo' => trim($this->input->post("Codigo")),
            ':Id_CentroEscolar' => strtoupper(trim($this->input->post("Id")))
        );
        $save = $this->centrosescolares->update($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se edito un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }

    public function remove()
    {
        $info = array(
            ':Id_CentroEscolar' => trim($this->uri->segment(4)),
            ':Eliminado' => 1
        );
        $save = $this->centrosescolares->remove($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se elimino un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }

}
?>

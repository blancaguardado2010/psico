<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GradoscentrosController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('gradoscentros_model', 'gradoscentros', 'm-centro');
        $this->load->library('menu');
        $this->load->helper('menu', 'menu_helper');
    }

    public function index()
    {
        $data["js"] = array(
            $this->library->modulosjs("loader","m-centro","gradoscentros"),
            $this->library->modulosjs("main"));
        $data['css'] = array(
            $this->library->vendorcss("kendo.bootstrap-v4.min","kendo","css")
        );
        $data['tabla'] = "m-centro/gradoscentros";
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $data['title'] = "Modulo de Grados Centros";
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('views', $data);
        $this->load->view('templates/footer', $data);
    }
    public function all()
    {
        $data = $this->gradoscentros->get_consulta();
        echo json_encode($data);
    }
    public function modal()
    {
        $Id = $this->uri->segment(4);

        if ($Id > 0 ) {
            $data['data'] = $this->gradoscentros->get_id($Id);
            $this->load->view("modals/m-centro/gradoscentros",$data);
        }else{
            $this->load->view("modals/m-centro/gradoscentros");
        }
    }
    public function save()
    {
        $info = array(
            ':Nombre' => trim($this->input->post("Nombre"))
        );
        $save = $this->gradoscentros->save($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se agrego con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    public function update()
    {
        $info = array(
            ':Nombre' => trim($this->input->post("Nombre")),
            ':Id_Grado_Centro' => strtoupper(trim($this->input->post("Id")))
        );
        $save = $this->gradoscentros->update($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se edito un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
    public function remove()
    {
        $info = array(
            ':Id_Grado_Centro' => trim($this->uri->segment(4)),
            ':Eliminado' => 1
        );
        $save = $this->gradoscentros->remove($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se elimino un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }

}
?>

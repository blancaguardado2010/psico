<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AlumnosController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('alumnos_model', 'alumnos', 'm-alumnos');
        $this->load->model('religiones_model', 'religiones', 'm-generales');

        $this->load->library('menu');
        $this->load->helper('menu', 'menu_helper');
    }

    public function index()
    {
        $data["js"] = array(
            $this->library->modulosjs("loader","m-alumnos","alumnos"),
            $this->library->modulosjs("main"));
        $data['css'] = array(
            $this->library->vendorcss("kendo.bootstrap-v4.min","kendo","css")
        );
        $data['tabla'] = "m-alumnos/alumnos";
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $data['title'] = "Modulo de alumnos";
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('views', $data);
        $this->load->view('templates/footer', $data);
    }
    public function all()
    {
        $data = $this->alumnos->get_consulta();
        echo json_encode($data);
    }
    public function modal()
    {
        $Id = $this->uri->segment(4);
        $data['religiones'] = $this->religiones->get_consulta();

        if ($Id > 0 ) {
            $data['data'] = $this->alumnos->get_id($Id);
            $this->load->view("modals/m-alumnos/alumnos",$data);
        }else{
            $this->load->view("modals/m-alumnos/alumnos",$data);
        }
    }
    public function save()
    {
        $info = array(
            ':Vive_Con' => trim($this->input->post("Vive_Con")),
            ':Id_Religion' => trim($this->input->post("Id_Religion")),
            ':Apellido' => trim($this->input->post("Apellido")),
            ':Nombre' => trim($this->input->post("Nombre")),
            ':Direccion' => trim($this->input->post("Direccion")),
            ':Telefono' => trim($this->input->post("Telefono")),
            ':FechaNacimiento' => trim($this->input->post("FechaNacimiento")),



        );
        $save = $this->alumnos->save($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se agrego con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
     public function update()
    {
        $info = array(
            ':Vive_Con' => trim($this->input->post("Vive_Con")),

           ':Id_Religion' => trim($this->input->post("Id_Religion")),
            ':Apellido' => trim($this->input->post("Apellido")),
            ':Nombre' => trim($this->input->post("Nombre")),
            ':Direccion' => trim($this->input->post("Direccion")),
            ':Telefono' => trim($this->input->post("Telefono")),
            ':FechaNacimiento' => trim($this->input->post("FechaNacimiento")),
            ':Id_Alumno' =>trim($this->input->post("Id"))
        );
        $save = $this->alumnos->update($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se edito un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }

    public function remove()
    {
        $info = array(
            ':Id_Religion' => trim($this->uri->segment(4)),
            ':Eliminado' => 1
        );
        $save = $this->alumnos->remove($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se elimino un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }

}
?>

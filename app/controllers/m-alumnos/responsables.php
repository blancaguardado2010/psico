<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ResponsablesController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('responsables_model', 'responsables', 'm-alumnos');
        $this->load->model('ocupaciones_model', 'ocupaciones', 'm-generales');

        $this->load->library('menu');
        $this->load->helper('menu', 'menu_helper');
    }

    public function index()
    {
        $data["js"] = array(
            $this->library->modulosjs("loader","m-alumnos","responsables"),
            $this->library->modulosjs("main"));
        $data['css'] = array(
            $this->library->vendorcss("kendo.bootstrap-v4.min","kendo","css")
        );
        $data['tabla'] = "m-alumnos/responsables";
        $data['sidebarMenu'] = $this->menu->render($this->menu_helper->GetMenu());
        $data['title'] = "Modulo de responsables";
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebars', $data);
        $this->load->view('views', $data);
        $this->load->view('templates/footer', $data);
    }
    public function all()
    {
        $data = $this->responsables->get_consulta();
        echo json_encode($data);
    }
    public function modal()
    {
        $Id = $this->uri->segment(4);
        $data['ocupaciones'] = $this->ocupaciones->get_consulta();

        if ($Id > 0 ) {
            $data['data'] = $this->responsables->get_id($Id);
            $this->load->view("modals/m-alumnos/responsables",$data);
        }else{
            $this->load->view("modals/m-alumnos/responsables",$data);
        }
    }
    public function save()
    {
        $info = array(
            ':Id_Ocupacion' => trim($this->input->post("Id_Ocupacion")),
            ':Dui' => trim($this->input->post("Dui")),
            ':Lugar_trabajo' => trim($this->input->post("Lugar_trabajo")),
            ':Apellido' => trim($this->input->post("Apellido")),
            ':Nombre' => trim($this->input->post("Nombre")),
            ':Direccion' => trim($this->input->post("Direccion")),
            ':Telefono_Trabajo' => trim($this->input->post("Telefono_Trabajo")),
            ':Fecha_Nacimiento' => trim($this->input->post("Fecha_Nacimiento")),



        );
        $save = $this->responsables->save($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se agrego con exito!,success,top,center";
            echo json_encode($datos);
        }
    }
     public function update()
    {
        $info = array(
            ':Id_Ocupacion' => trim($this->input->post("Id_Ocupacion")),
            ':Dui' => trim($this->input->post("Dui")),
            ':Lugar_trabajo' => trim($this->input->post("Lugar_trabajo")),
            ':Apellido' => trim($this->input->post("Apellido")),
            ':Nombre' => trim($this->input->post("Nombre")),
            ':Direccion' => trim($this->input->post("Direccion")),
            ':Telefono_Trabajo' => trim($this->input->post("Telefono_Trabajo")),
            ':Fecha_Nacimiento' => trim($this->input->post("Fecha_Nacimiento")),
            ':Id_Responsable' =>trim($this->input->post("Id"))
        );
        $save = $this->responsables->update($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se edito un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }

    public function remove()
    {
        $info = array(
            ':Id_Responsable' => trim($this->uri->segment(4)),
            ':Eliminado' => 1
        );
        $save = $this->responsables->remove($info);
        if ($save != 0)
        {
            $datos = "Error,Ocurrio un error!,danger,top,center";
            echo json_encode($datos);
        }
        else
        {
            $datos = "success,Se elimino un registro con exito!,success,top,center";
            echo json_encode($datos);
        }
    }

}
?>

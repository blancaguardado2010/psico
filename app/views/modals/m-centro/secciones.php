
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="modal_global">Secciones</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
		 <div class="modal-body">
            <form datos="usuarios" method='POST' role='form' class='add' data-target="<?= site_url('m-centro/secciones/'.$valor = (isset($data['Id_Seccion']))? 'update/' : 'save/')  ?>" enctype='multipart/form-data'>
				<div class='form-group'>
                    <label>Digite un seccion</label>
                    <input type='text' name='Nombre' class='form-control' value="<?= $valor = (isset($data['Nombre']))? $data['Nombre'] : '' ?>"  placeholder='Seccion' data-target-ms='El campo Rol es obligatorio' required><br>
                </div>
                <input type="hidden" name="Id" value="<?= $valor = (isset($data['Id_Seccion']))? $data['Id_Seccion'] : '0' ?>">
            </form>
        </div>
		<div class="modal-footer">
		    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		    <button type="button" class="btn btn-primary save">Guardar Datos</button>
		</div>
    </div>
</div>
<script src='<?=  base_url(); ?>assets/modulosjs/main.js'></script>


<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="modal_global">Centros Escolares</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
		 <div class="modal-body">
            <form datos="usuarios" method='POST' role='form' class='add' data-target="<?= site_url('m-centro/centrosescolares/'.$valor = (isset($data['Id_CentroEscolar']))? 'update/' : 'save/')  ?>" enctype='multipart/form-data'>
				 <div class='form-group'>
                    <label>Digite un Nombre</label>
                    <input type='text' name="Nombre" class='form-control' value="<?= (isset($data['Nombre']))? $data['Nombre'] : '' ?>"  placeholder='Nombre' data-target-ms='El campo Nombre es obligatorio' <?= (isset($data['Nombre']))? '' : 'required' ?>><br>
                </div>
                <div class='form-group'>
                    <label>Digite la Direccion</label>
                    <input type='text' name='Direccion' class='form-control' value="<?= (isset($data['Direccion']))? $data['Direccion'] : '' ?>"  placeholder='Direccion' data-target-ms='El campo Direccion es obligatorio' <?= (isset($data['Direccion']))? '' : 'required' ?>><br>
                </div>
                  <div class='form-group'>
                    <label>Digite EL Telefono </label>
                    <input type='text' name='Telefono' class='form-control Telefono' value="<?= (isset($data['Telefono']))? $data['Telefono'] : '' ?>"  placeholder='Telefono' data-target-ms='El campo Direccion es obligatorio' <?= (isset($data['Telefono']))? '' : 'required' ?>><br>
                </div>
               <div class='form-group'>
                    <label>Seleccione un Rol</label>
                    <select name="Id_Encargado" class="form-control selectpicker" data-live-search="true">
                        <?php foreach ($encargados as $row): ?>
                            <option value="<?= $row['Id_Encargado'] ?>" <?= (@$data['Id_Encargado'] == $row['Id_Encargado'])? 'selected' : '' ?> ><?= $row['Nombre'] ?></option>
                        <?php endforeach ?>
                    </select>
                    <br>
                </div>
                <div class='form-group'>
                    <label>Digite el codigo del cenetro escolar</label>
                    <input type='text' name='Codigo' class='form-control numero' value=""  placeholder='Codigo' data-target-ms='El campo Direccion es obligatorio' <?= (isset($data['Codigo']))? '' : 'required' ?>><br>
                </div>
                <div class='form-group'>
                    <label>Seleccione un Municipio</label>
                    <select name="Id_Municipio" class="form-control selectpicker" data-live-search="true">
                        <?php foreach ($municipios as $row): ?>
                            <option value="<?= $row['Id_Municipio'] ?>" <?= (@$data['Id_Municipio'] == $row['Id_Municipio'])? 'selected' : '' ?> ><?= $row['Nombre'] ?></option>
                        <?php endforeach ?>
                    </select>

                    <br>
                </div>
                <input type="hidden" name="Id" value="<?= $valor = (isset($data['Id_CentroEscolar']))? $data['Id_CentroEscolar'] : '0' ?>">
            </form>
        </div>
		<div class="modal-footer">
		    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		    <button type="button" class="btn btn-primary save">Guardar Datos</button>
		</div>
    </div>
</div>
<script src='<?=  base_url(); ?>assets/modulosjs/main.js'></script>


<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="modal_global">test</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
		 <div class="modal-body">
            <form datos="test" method='POST' role='form' class='add' data-target="<?= site_url('m-test/test/'.$valor = (isset($data['Id_test']))? 'update/' : 'save/')  ?>" enctype='multipart/form-data'>
                <div class="row">
                    <div class='col-md-12'> 
                        <div class='form-group'>
		                    <label for='Test'>Digite un Test</label>
		                    <input type='text' id='Test' name='Test' class='form-control' value="<?= (isset($data['Test']))? $data['Test'] : '' ?>"  placeholder='Test' data-target-ms='El campo Test es obligatorio' required><br>
		                </div>
                        <div class='form-group'>
		                    <label for='Id_tipo_test'>Seleccione un Tipo de Test</label>
                            <select name="Id_tipo_test" class="form-control" id="Id_tipo_test">
                                <?php foreach ($tipotest as $row): ?>
                                    <option value="<?= $row['Id_tipo_test'] ?>" <?= (@$data['Id_tipo_test'] == $row['Id_tipo_test'])? 'selected' : '' ?> ><?= $row['Tipos_test'] ?></option>
                                <?php endforeach ?>
                            </select>
                            <br>
		                </div> 
                    </div>
                </div>
                <input type="hidden" name="Id" value="<?= $valor = (isset($data['Id_test']))? $data['Id_test'] : '0' ?>">
            </form>
        </div>
		<div class="modal-footer">
		    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		    <button type="button" class="btn btn-primary save">Guardar Datos</button>
		</div>
    </div>
</div>
<script src='<?=  base_url(); ?>assets/modulosjs/main.js'></script>
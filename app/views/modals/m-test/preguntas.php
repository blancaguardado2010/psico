<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="modal_global">Agregar preguntas</h5>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-9"></div>
                    <div class="col-md-3">
                        <span class="btn btn-info contador-pre" contador="0" dato="<?= $id ?>" onclick="add_html($(this),$('.add-html'),'add_preguntas');">Agregar pregunta <i class="fas fa-plus-circle"></i></span>
                    </div>
                </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
		<div class="modal-body pregun" style="max-height: 450px !important; overflow-y:scroll;" >
            <form datos="test" method='POST' role='form' class='add preguntas-add' data-target="<?= site_url('m-test/test/'.$valor = (isset($data['Id_test']))? 'update/' : 'save/')  ?>" enctype='multipart/form-data'>
                <div class="row">
                    <div class="col-md-12 add-html">
                        <h3 class="text-center info"><b>No asignado ninguna pregunta</b></h3>
                    </div>
                </div>    
                <input type="hidden" name="Id" value="<?= $valor = (isset($data['Id_test']))? $data['Id_test'] : '0' ?>">
            </form>
        </div>
		<div class="modal-footer">
		    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		    <button type="button" class="btn btn-primary save">Guardar Datos</button>
		</div>
    </div>
</div>
<script src='<?=  base_url(); ?>assets/modulosjs/main.js'></script>

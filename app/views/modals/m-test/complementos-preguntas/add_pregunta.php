<div class="row pre-<?= $pos ?>">
    <div class="col-md-10 cont-<?= $pos ?>">
        <div class="row">
            <div class='col-md-9'>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><?= $pos ?></span>
                    </div>
                    <input type='text' id='Pregunta' name='Pregunta' class='form-control Pregunta-<?= $pos ?>' value="<?= (isset($data['Pregunta']))? $data['Pregunta'] : '' ?>"  placeholder='Pregunta' data-target-ms='El campo Pregunta es obligatorio' required>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group'>
                    <select name="Id_tipo_test" class="form-control Id_tipo_test-<?= $pos ?>" id="Id_tipo_test">
                        <option value="">Seleccione</option>
                        <option value="0">Opcional</option>
                        <option value="1">Escrita</option>
                    </select>
                    <br>
                </div>
            </div>
        </div>
    </div>            
    <div class="col-md-2 bottum-<?= $pos ?>">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <span class="btn btn-success mr-2" onclick="sheck(<?= $pos ?>)"><i class="fas fa-check"></i></span>
                    </div>
                </div>                            
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <span class="btn btn-danger mr-2" onclick="del_html($('.pre-<?= $pos ?>'),$('.contador-pre'))"><i class="fas fa-minus-circle"></i></span>
                    </div>
                </div>                            
            </div>
        </div>
    </div>
</div>
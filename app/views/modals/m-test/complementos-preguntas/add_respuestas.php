<div class="row res-<?= $pos ?>">
    <div class="col-md-2">
        <div class='form-group'>
            <input type="text" name="Literal[]" placeholder="Literal" class="form-control">
        </div>
    </div>
    <div class="col-md-8">
        <div class='form-group'>
            <input type="text" name="Respuesta[]" placeholder="Respuesta" class="form-control">
        </div>
    </div>
    <div class="col-md-1">
        <div class='form-group'>
            <input type="radio" name="Correcta">
        </div>
    </div>
    <div class="col-md-1">
        <div class="row">
            <div class="col-md-12">
                <span class="btn btn-danger mr-2" onclick="del_html($('.res-<?= $pos ?>'),$('.contador-res'),1)"><i class="fas fa-minus-circle"></i></span>
            </div>
        </div>                            
    </div>
</div>
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="modal_global">Agregar respuestas</h5>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-9"></div>
                    <div class="col-md-3">
                        <span class="btn btn-info contador-res" contador="1" onclick="add_html($(this),$('.add-res'),'add_respuestas',1);">Agregar respuesta <i class="fas fa-plus-circle"></i></span>
                    </div>
                </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
		<div class="modal-body" style="max-height: 450px !important; overflow-y:scroll;">
            <form datos="test" method='POST' role='form' class='add' data-target="<?= site_url('m-test/test/'.$valor = (isset($data['Id_test']))? 'update/' : 'save/')  ?>" enctype='multipart/form-data'>
               <div class="row">
                    <div class="col-md-12 add-res">
                        <div class="row res-1">
                            <div class="col-md-2">
                                <div class='form-group'>
                                    <label for='Id_tipo_test'>Literal</label>
                                    <input type="text" name="Literal[]" placeholder="Literal" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class='form-group'>
                                    <label for='Id_tipo_test'>Respuesta</label>
                                    <input type="text" name="Respuesta[]" placeholder="Respuesta" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label for='Id_tipo_test'>Correcta</label>
                                <div class='form-group'>
                                    <input type="radio" name="Correcta">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="Id" value="<?= $valor = (isset($data['Id_test']))? $data['Id_test'] : '0' ?>">
            </form>
        </div>
		<div class="modal-footer">
		    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		    <button type="button" class="btn btn-primary save">Guardar Respuestas</button>
		</div>
    </div>
</div>
<script src='<?=  base_url(); ?>assets/modulosjs/main.js'></script>

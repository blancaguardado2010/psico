
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="modal_global">tipos_test</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
		 <div class="modal-body">
            <form datos="tipos_test" method='POST' role='form' class='add' data-target="<?= site_url('m-test/tipotest/'.$valor = (isset($data['Id_tipo_test']))? 'update/' : 'save/')  ?>" enctype='multipart/form-data'>
                <div class="row">
                    <div class='col-md-12'> 
                        <div class='form-group'>
    	                    <label for='Tipos_test'>Digite un Tipos test</label>
    	                    <input type='text' id='Tipos_test' name='Tipos_test' class='form-control' value="<?= (isset($data['Tipos_test']))? $data['Tipos_test'] : '' ?>"  placeholder='Tipos_test' data-target-ms='El campo Tipos_test es obligatorio' required><br>
    	                </div> 
                    </div>
                </div>
                <input type="hidden" name="Id" value="<?= $valor = (isset($data['Id_tipo_test']))? $data['Id_tipo_test'] : '0' ?>">
            </form>
        </div>
		<div class="modal-footer">
		    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		    <button type="button" class="btn btn-primary save">Guardar Datos</button>
		</div>
    </div>
</div>
<script src='<?=  base_url(); ?>assets/modulosjs/main.js'></script>
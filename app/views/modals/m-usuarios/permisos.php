
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="modal_global">permisos</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
         <div class="modal-body">

            <form datos="usuarios" method='POST' role='form' class='add' data-target="<?= site_url('m-usuarios/permisos/'.$valor = (isset($data['Id_Permisos']))? 'update/' : 'save/')  ?>" enctype='multipart/form-data'>
                <div class='form-group'>
                    <label>Digite un Nombre</label>
                    <input type='text' name='Nombre' class='form-control' value="<?= $valor = (isset($data['Nombre']))? $data['Nombre'] : '' ?>"  placeholder='Nombre' data-target-ms='El campo Rol es obligatorio' required><br>
                </div>
                <div class='form-group'>
                    <label>Digite numero de Permiso  </label>
                    <input type='text' name='Permiso' class='form-control' value="<?= $valor = (isset($data['Permiso']))? $data['Permiso'] : '' ?>"  placeholder='Permiso' data-target-ms='El campo Rol es obligatorio' required><br>
                </div>

                <input type="hidden" name="Id" value="<?= $valor = (isset($data['Id_Permisos']))? $data['Id_Permisos'] : '0' ?>">
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-primary save">Guardar Datos</button>
        </div>
    </div>
</div>
<script src='<?=  base_url(); ?>assets/modulosjs/main.js'></script>

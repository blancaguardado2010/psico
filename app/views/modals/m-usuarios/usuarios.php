
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="modal_global">usuarios</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
		 <div class="modal-body">
            <form datos="usuarios" method='POST' role='form' class='add' data-target="<?= site_url('m-usuarios/usuarios/'.$valor = (isset($data['Id_Usuario']))? 'update/' : 'save/')  ?>" enctype='multipart/form-data'>
				<div class='form-group'>
                    <label>Digite un User</label>
                    <input type='text' name='User' class='form-control' value="<?= $valor = (isset($data['User']))? $data['User'] : '' ?>"  placeholder='User' data-target-ms='El campo User es obligatorio' required><br>
                </div>
                <div class='form-group'>
                    <label>Digite un Password</label>
                    <input type='password' name='Password' class='form-control' value=""  placeholder='Password' data-target-ms='El campo Password es obligatorio' <?= (isset($data['Pasword']))? '' : 'required' ?>><br>
                </div>
                <div class='form-group'>
                    <label>Seleccione un Rol</label>
                    <select name="Id_Rol" class="form-control selectpicker" data-live-search="true">
                        <?php foreach ($roles as $row): ?>
                            <option value="<?= $row['Id_Rol'] ?>" <?= (@$data['Id_Rol'] == $row['Id_Rol'])? 'selected' : '' ?> ><?= $row['Nombre'] ?></option>
                        <?php endforeach ?>
                    </select>
                    <br>
                </div>
                <div class='form-group'>
                    <label>Seleccione un Estado</label>
                    <select name="Id_Estado" class="form-control selectpicker" data-live-search="true">
                        <?php foreach ($estados as $row): ?>
                            <option value="<?= $row['Id_Estado'] ?>" <?= (@$data['Id_Estado'] == $row['Id_Estado'])? 'selected' : '' ?> ><?= $row['Nombre'] ?></option>
                        <?php endforeach ?>
                    </select>
                    <br>
                </div>
                <input type="hidden" name="Id" value="<?= $valor = (isset($data['Id_Usuario']))? $data['Id_Usuario'] : '0' ?>">
            </form>
        </div>
		<div class="modal-footer">
		    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		    <button type="button" class="btn btn-primary save">Guardar Datos</button>
		</div>
    </div>
</div>
<script src='<?=  base_url(); ?>assets/modulosjs/main.js'></script>

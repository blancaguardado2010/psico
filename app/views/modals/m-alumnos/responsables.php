
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="modal_global">Centros Escolares</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
		 <div class="modal-body">
            <form datos="usuarios" method='POST' role='form' class='add' data-target="<?= site_url('m-alumnos/responsables/'.$valor = (isset($data['Id_Responsable']))? 'update/' : 'save/')  ?>" enctype='multipart/form-data'>
				 <div class='form-group'>
                    <label>Digite un Nombre</label>
                    <input type='text' name="Nombre" class='form-control' value="<?= (isset($data['Nombre']))? $data['Nombre'] : '' ?>"  placeholder='Nombre' data-target-ms='El campo Nombre es obligatorio' <?= (isset($data['Nombre']))? '' : 'required' ?>><br>
                </div>
                <div class='form-group'>
                    <label>Digite la Direccion</label>
                    <input type='text' name='Direccion' class='form-control' value="<?= (isset($data['Direccion']))? $data['Direccion'] : '' ?>"  placeholder='Direccion' data-target-ms='El campo Direccion es obligatorio' <?= (isset($data['Direccion']))? '' : 'required' ?>><br>
                </div>
                  <div class='form-group'>
                    <label>Digite EL Telefono </label>
                    <input type='text' name='Telefono' class='form-control Telefono' value="<?= (isset($data['Telefono']))? $data['Telefono'] : '' ?>"  placeholder='Telefono' data-target-ms='El campo Direccion es obligatorio' <?= (isset($data['Telefono']))? '' : 'required' ?>><br>
                </div>
                <div class='form-group'>
                    <label>Digite EL Lugar_trabajo</label>
                    <input type='text' name='Lugar_trabajo' class='form-control ' value="<?= (isset($data['Lugar_trabajo']))? $data['Lugar_trabajo'] : '' ?>"  placeholder='Lugar_trabajo' data-target-ms='El campo Direccion es obligatorio' <?= (isset($ata['Lugar_trabajo']))? '' : 'required' ?>><br>
                </div>
               <div class='form-group'>
                    <label>Seleccione una Religion</label>
                    <select name="IId_Ocupacion" class="form-control">
                        <?php foreach ($ocupaciones as $row): ?>
                            <option value="<?= $row['Id_Ocupacion'] ?>" <?= (@$data['Id_Ocupacion'] == $row['Id_Ocupacion'])? 'selected' : '' ?> ><?= $row['Nombre'] ?></option>
                        <?php endforeach ?>
                    </select>
                    <br>
                </div>
                <div class='form-group'>
                    <label>Digite EL Telefono </label>
                    <input type='date' name='Fecha_Nacimiento' class='form-control Telefono' value="<?= (isset($data['Fecha_Nacimiento']))? $data['Fecha_Nacimiento'] : '' ?>"  placeholder='Fecha_Nacimiento' data-target-ms='El campo Direccion es obligatorio' <?= (isset($data['Fecha_Nacimiento']))? '' : 'required' ?>><br>
                </div>
                <div class='form-group'>
                    <label>Digite el codigo vive con</label>
                    <input type='text' name='Dui' class='form-control numero' value="<?= (isset($data['Dui']))? $data['Dui'] : '' ?>"  placeholder='Dui' data-target-ms='El campo Direccion es obligatorio' <?= (isset($data['Dui']))? '' : 'required' ?>><br>
                </div>

                <input type="hidden" name="Id" value="<?= $valor = (isset($data['Id_Responsable']))? $data['Id_Responsable'] : '0' ?>">
            </form>
        </div>
		<div class="modal-footer">
		    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		    <button type="button" class="btn btn-primary save">Guardar Datos</button>
		</div>
    </div>
</div>
<script src='<?=  base_url(); ?>assets/modulosjs/main.js'></script>

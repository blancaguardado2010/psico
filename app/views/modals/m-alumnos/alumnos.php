





<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="modal_global">Centros Escolares</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
		 <div class="modal-body">
            <form datos="usuarios" method='POST' role='form' class='add' data-target="<?= site_url('m-alumnos/alumnos/'.$valor = (isset($data['Id_Alumno']))? 'update/' : 'save/')  ?>" enctype='multipart/form-data'>
				 <div class='form-group'>
                    <label>Digite un Nombre</label>
                    <input type='text' name="Nombre" class='form-control' value="<?= (isset($data['Nombre']))? $data['Nombre'] : '' ?>"  placeholder='Nombre' data-target-ms='El campo Nombre es obligatorio' <?= (isset($data['Nombre']))? '' : 'required' ?>><br>
                </div>
                <div class='form-group'>
                    <label>Digite la Direccion</label>
                    <input type='text' name='Direccion' class='form-control' value="<?= (isset($data['Direccion']))? $data['Direccion'] : '' ?>"  placeholder='Direccion' data-target-ms='El campo Direccion es obligatorio' <?= (isset($data['Direccion']))? '' : 'required' ?>><br>
                </div>
                  <div class='form-group'>
                    <label>Digite EL Telefono </label>
                    <input type='text' name='Telefono' class='form-control Telefono' value="<?= (isset($data['Telefono']))? $data['Telefono'] : '' ?>"  placeholder='Telefono' data-target-ms='El campo Direccion es obligatorio' <?= (isset($data['Telefono']))? '' : 'required' ?>><br>
                </div>
               <div class='form-group'>
                    <label>Seleccione una Religion</label>
                    <select name="Id_Religion" class="form-control">
                        <?php foreach ($religiones as $row): ?>
                            <option value="<?= $row['Id_Religion'] ?>" <?= (@$data['Id_Religion'] == $row['Id_Religion'])? 'selected' : '' ?> ><?= $row['Nombre'] ?></option>
                        <?php endforeach ?>
                    </select>
                    <br>
                </div>
                <div class='form-group'>
                    <label>Digite EL fecha </label>
                    <input type='date' name='FechaNacimiento' class='form-control' value="<?= (isset($data['FechaNacimiento']))? $data['FechaNacimiento'] : '' ?>"  placeholder='FechaNacimiento' data-target-ms='El campo Direccion es obligatorio' required><br>
                </div>
                 <div class='form-group'>
                    <label>Vive_Con </label>
                    <input type='text' name='Vive_Con' class='form-control' value="<?= (isset($data['Vive_Con']))? $data['Vive_Con'] : '' ?>"  placeholder='Vive_Con' data-target-ms='El campo Direccion es obligatorio' <?= (isset($data['Vive_Con']))? '' : 'required' ?>><br>
                </div>

                

                <input type="hidden" name="Id" value="<?= $valor = (isset($data['Id_Alumno']))? $data['Id_Alumno'] : '0' ?>">
            </form>
        </div>
		<div class="modal-footer">
		    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		    <button type="button" class="btn btn-primary save">Guardar Datos</button>
		</div>
    </div>
</div>
<script src='<?=  base_url(); ?>assets/modulosjs/main.js'></script>

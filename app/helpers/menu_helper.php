<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MenuHelper
{
    public function GetMenu()
    {
    	$Menu = array(
          "Usuarios" => array(
              "icon" => 'fas fa-user',
              'items' => array(
                "Usuarios" => "m-usuarios/usuarios",
                "Roles" => "m-usuarios/roles",
                "Permisos" => "m-usuarios/permisos"
              )
          ),
          "Centro escolar" => array(
              "icon" => 'fas fa-graduation-cap',
              'items' => array(
                "Centros Escolares" => "m-centro/centrosescolares",
                "Grados centros" => "m-centro/gradoscentros",
                "Docentes" => "m-centro/docentes",
                "Encargados centro" => "m-centro/encargados",
                "Secciones" => "m-centro/secciones",
                "Turno" => "m-centro/turno"
              )
          ),
          "Alumnos" => array(
              "icon" => 'fas fa-users',
              'items' => array(
                "Alumnos" => "m-alumnos/alumnos",
                "responsables" => "m-alumnos/responsables"
              )
          ),
          "Datos Generales" => array(
              "icon" => 'fas fa-list-ol',
              'items' => array(
                "Municipios" => "m-generales/municipios",
                "Religion" => "m-generales/religiones",
                "Parentesco" => "m-generales/parentescos",
                "Ocupaciones" => "m-generales/ocupaciones"
              )
          ),
          "Config Test" => array(
		        "icon" => 'fas fa-file-alt',
		        'items' => array(
                "Tipos de Test" => "m-test/tipotest",
                "Test" => "m-test/test",
                "Preguntas Test" => "m-test/preguntastest"
            )
  			  ),
          "Realizar Test" => array(
		        "icon" => 'fas fa-edit',
            'page' => 'm-test/frontendtest'
  			  ),
          "Estados" => array(
            "icon" => 'fas fa-calendar-check',
            'page' => 'estados'
          )
			 );

        return $Menu;
    }
}?>

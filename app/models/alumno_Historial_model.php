<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Alumno_HistorialModel extends Model
{
    public function __construct()
    {
        parent::__construct(array('default'));   
    }

    
    public function get_id($Id)
    {
        $params = array(":Id_Alumno_Historial" => $Id);
        $query = "SELECT * FROM alumno_Historial WHERE Id_Alumno_Historial = :Id_Alumno_Historial ";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetch();
    }
    public function save($data)
    {
        try 
        {
            $query = "INSERT INTO alumno_Historial(Id_Alumno,Id_Responsable,Id_Parentezco) VALUES(:Id_Alumno,:Id_Responsable,:Id_Parentezco)";
            $q = $this->default->prepare($query);             
            $q->execute($data);
            return 0;
        } 
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }

    public function update($data)
    {
        try 
        {
            $query = "UPDATE alumno_Historial SET Id_Alumno =:Id_Alumno,Id_Responsable=:Id_Responsable,Id_Parentezco=:Id_Parentezco WHERE Id_Alumno_Historial = :Id_Alumno_Historial";
            $q = $this->default->prepare($query);
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
    
    
}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MunicipiosModel extends Model
{
    public function __construct()
    {
        parent::__construct(array('default'));   
    }

    public function get_consulta()
    {
        $query = "SELECT * FROM municipios WHERE Eliminado = 0";
        $result = $this->default->prepare($query);
        $result->execute();
        return $result->fetchAll();
    }
    public function get_id($Id)
    {
        $params = array(":Id_Municipio" => $Id);
        $query = "SELECT * FROM municipios WHERE Id_Municipio = :Id_Municipio AND Eliminado = 0";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetch();
    }
    public function save($data)
    {
        try 
        {
            $query = "INSERT INTO municipios(Nombre) VALUES(:Nombre)";
            $q = $this->default->prepare($query);             
            $q->execute($data);
            return 0;
        } 
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }

    public function update($data)
    {
        try 
        {
            $query = "UPDATE municipios SET Nombre = :Nombre WHERE Id_Municipio = :Id_Municipio";
            $q = $this->default->prepare($query);
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
    
    public function remove($data)
    {
        try 
        {
            $q = $this->default->prepare('UPDATE municipios SET Eliminado = :Eliminado WHERE Id_Municipio = :Id_Municipio');
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
}
?>
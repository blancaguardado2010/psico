<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class TestModel extends Model
{
    public function __construct()
    {
        parent::__construct(array('default'));   
    }

    public function get_consulta()
    {
        $query = "SELECT 
                    test.Id_test,
                    test.Test,
                    tipos_test.Tipos_test
                FROM test
                INNER JOIN tipos_test ON test.Id_tipo_test = tipos_test.Id_tipo_test
                WHERE test.Eliminado = 0";
        $result = $this->default->prepare($query);
        $result->execute();
        return $result->fetchAll();
    }
    public function get_id($Id)
    {
        $params = array(":Id_test" => $Id);
        $query = "SELECT * FROM test WHERE Id_test = :Id_test";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetch();
    }
    public function save($data)
    {
        try 
        {
            $query = "INSERT INTO test(Test,Id_tipo_test) VALUES(:Test,:Id_tipo_test)";
            $q = $this->default->prepare($query);             
            $q->execute($data);
            return 0;
        } 
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
    public function update($data)
    {
        try 
        {
            $query = "UPDATE test SET Test = :Test,Id_tipo_test = :Id_tipo_test WHERE Id_test = :Id_test";
            $q = $this->default->prepare($query);
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
    
    public function remove($data)
    {
        try 
        {
            $q = $this->default->prepare('UPDATE test SET Eliminado = :Eliminado WHERE Id_test= :Id_test');
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class PreguntastestModel extends Model
{
    public function __construct()
    {
        parent::__construct(array('default'));   
    }

    public function get_consulta()
    {
        $query = "SELECT * FROM preguntas_test WHERE Eliminado = 0";
        $result = $this->default->prepare($query);
        $result->execute();
        return $result->fetchAll();
    }
    public function get_id($Id)
    {
        $params = array(":Id_pregunta_test" => $Id);
        $query = "SELECT * FROM preguntas_test WHERE Id_pregunta_test = :Id_pregunta_test";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetch();
    }
    public function get_id_test($Id)
    {
        $params = array(":Id_test" => $Id);
        $query = "SELECT * FROM preguntas_test WHERE Id_test = :Id_test";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetchAll();
    }
    public function save($data)
    {
        try 
        {
            $query = "INSERT INTO preguntas_test(Pregunta,Id_test,Tipo_pregunta,Eliminado) VALUES(:Pregunta,:Id_test,:Tipo_pregunta,:Eliminado)";
            $q = $this->default->prepare($query);             
            $q->execute($data);
            return 0;
        } 
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
    public function update($data)
    {
        try 
        {
            $query = "UPDATE preguntas_test SET Pregunta = :Pregunta,Id_test = :Id_test,Tipo_pregunta = :Tipo_pregunta,Eliminado = :Eliminado WHERE Id_pregunta_test = :Id_pregunta_test";
            $q = $this->default->prepare($query);
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
    
    public function remove($data)
    {
        try 
        {
            $q = $this->default->prepare('UPDATE preguntas_test SET Eliminado = :Eliminado WHERE Id_pregunta_test= :Id_pregunta_test');
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class RespuestastestModel extends Model
{
    public function __construct()
    {
        parent::__construct(array('default'));   
    }

    public function get_consulta()
    {
        $query = "SELECT * FROM respuestas_test";
        $result = $this->default->prepare($query);
        $result->execute();
        return $result->fetchAll();
    }
    public function get_id($Id)
    {
        $params = array(":Id_respuesta_test" => $Id);
        $query = "SELECT * FROM respuestas_test WHERE Id_respuesta_test = :Id_respuesta_test";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetch();
    }
    public function save($data)
    {
        try 
        {
            $query = "INSERT INTO respuestas_test(Literal,Respuesta_test,Correcta,Eliminado) VALUES(:Literal,:Respuesta_test,:Correcta,:Eliminado)";
            $q = $this->default->prepare($query);             
            $q->execute($data);
            return 0;
        } 
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
    public function update($data)
    {
        try 
        {
            $query = "UPDATE respuestas_test SET Literal = :Literal,Respuesta_test = :Respuesta_test,Correcta = :Correcta,Eliminado = :Eliminado WHERE Id_respuesta_test = :Id_respuesta_test";
            $q = $this->default->prepare($query);
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
    
    public function remove($data)
    {
        try 
        {
            $q = $this->default->prepare('UPDATE respuestas_test SET Eliminado = :Eliminado WHERE Id_respuesta_test= :Id_respuesta_test');
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
}
?>
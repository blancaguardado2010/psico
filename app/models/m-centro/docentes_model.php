<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class docentesModel extends Model
{
    public function __construct()
    {
        parent::__construct(array('default'));   
    }

    public function get_consulta()
    {
        $query = "SELECT * FROM docentes WHERE Eliminado = 0";
        $result = $this->default->prepare($query);
        $result->execute();
        return $result->fetchAll();
    }
    public function get_id($Id)
    {
        $params = array(":Id_docente" => $Id);
        $query = "SELECT * FROM docentes WHERE Id_docente = :Id_docente AND Eliminado = 0";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetch();
    }
    public function save($data)
    {
        try 
        {
            $query = "INSERT INTO docentes(Nombre,Telefono,Direccion) VALUES(:Nombre,:Telefono,:Direccion)";
            $q = $this->default->prepare($query);             
            $q->execute($data);
            return 0;
        } 
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }

    public function update($data)
    {
        try 
        {
            $query = "UPDATE docentes SET Nombre = :Nombre, =:,Telefono=:Telefono,Direccion=:Direccion 
             WHERE Id_docente = :Id_docente";




            $query = "UPDATE docentes SET  Nombre = :Nombre,Telefono=:Telefono,Direccion=:Direccion WHERE Id_docente = :Id_docente";
            $q = $this->default->prepare($query);
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
    
    public function remove($data)
    {
        try 
        {
            $q = $this->default->prepare('UPDATE docentes SET Eliminado = :Eliminado WHERE Id_docente = :Id_docente');
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
}
?>
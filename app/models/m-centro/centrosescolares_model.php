<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class centrosescolaresModel extends Model
{
    public function __construct()
    {
        parent::__construct(array('default'));   
    }

    public function get_consulta()
    {
        $query = "SELECT 
                        centrosescolares.Id_CentroEscolar,
                        centrosescolares.Nombre,
                        centrosescolares.Direccion ,
                        centrosescolares.Telefono ,
                        centrosescolares.Codigo,
                        encargados.Nombre AS Encargado,
                        municipios.Nombre AS Municipio

                    FROM centrosescolares
                    INNER JOIN encargados ON centrosescolares.Id_Encargado = encargados.Id_Encargado
                    INNER JOIN municipios ON centrosescolares.Id_Municipio = municipios.Id_Municipio 
                    WHERE centrosescolares.Eliminado = 0";
        $result = $this->default->prepare($query);
        $result->execute();
        return $result->fetchAll();
    }
    public function get_id($Id)
    {
        $params = array(":Id_CentroEscolar" => $Id);
        $query = "SELECT * FROM centrosescolares WHERE Id_CentroEscolar = :Id_CentroEscolar AND Eliminado = 0";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetch();
    }
    public function save($data)
    {
        try 
        {
            $query = "INSERT INTO centrosescolares(Id_Encargado,Id_Municipio,Nombre,Direccion,Telefono,Codigo) VALUES(:Id_Encargado,:Id_Municipio,:Nombre,:Direccion,:Telefono,:Codigo)";
            $q = $this->default->prepare($query);             
            $q->execute($data);
            return 0;
        } 
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }

    public function update($data)
    {
        try 
        {
            $query = "UPDATE centrosescolares SET  Id_Encargado = :Id_Encargado,  Id_Municipio = :Id_Municipio, Nombre = :Nombre, Direccion=:Direccion, Telefono =:Telefono, Codigo=:Codigo  WHERE Id_CentroEscolar = :Id_CentroEscolar";
            $q = $this->default->prepare($query);
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }

    
    
    public function remove($data)
    {
        try 
        {
            $q = $this->default->prepare('UPDATE centrosescolares SET Eliminado = :Eliminado WHERE Id_CentroEscolar = :Id_CentroEscolar');
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
}
?>
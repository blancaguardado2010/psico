<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SeccionesModel extends Model
{
    public function __construct()
    {
        parent::__construct(array('default'));   
    }

    public function get_consulta()
    {
        $query = "SELECT * FROM secciones WHERE Eliminado = 0";
        $result = $this->default->prepare($query);
        $result->execute();
        return $result->fetchAll();
    }
    public function get_id($Id)
    {
        $params = array(":Id_Seccion" => $Id);
        $query = "SELECT * FROM secciones WHERE Id_Seccion = :Id_Seccion AND Eliminado = 0";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetch();
    }
    public function save($data)
    {
        try 
        {
            $query = "INSERT INTO secciones(Nombre) VALUES(:Nombre)";
            $q = $this->default->prepare($query);             
            $q->execute($data);
            return 0;
        } 
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }

    public function update($data)
    {
        try 
        {
            $query = "UPDATE secciones SET Nombre = :Nombre WHERE Id_Seccion = :Id_Seccion";
            $q = $this->default->prepare($query);
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
    
    public function remove($data)
    {
        try 
        {
            $q = $this->default->prepare('UPDATE secciones SET Eliminado = :Eliminado WHERE Id_Seccion = :Id_Seccion');
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
}
?>
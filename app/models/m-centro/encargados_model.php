<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class EncargadosModel extends Model
{
    public function __construct()
    {
        parent::__construct(array('default'));   
    }

    public function get_consulta()
    {
        $query = "SELECT * FROM encargados WHERE Eliminado = 0";
        $result = $this->default->prepare($query);
        $result->execute();
        return $result->fetchAll();
    }
    public function get_id($Id)
    {
        $params = array(":Id_Encargado" => $Id);
        $query = "SELECT * FROM encargados WHERE Id_Encargado = :Id_Encargado AND Eliminado = 0";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetch();
    }
    public function save($data)
    {
        try 
        {
            $query = "INSERT INTO encargados(Nombre,Dui,Telefono,Correo) VALUES(:Nombre,:Dui,:Telefono,:Correo)";
            $q = $this->default->prepare($query);             
            $q->execute($data);
            return 0;
        } 
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }

    public function update($data)
    {
        try 
        {
            $query = "UPDATE encargados SET Nombre = :Nombre, Dui=:Dui,Telefono=:Telefono,Correo=:Correo 
             WHERE Id_Encargado = :Id_Encargado";




            $query = "UPDATE encargados SET  Nombre = :Nombre, Dui=:Dui,Telefono=:Telefono,Correo=:Correo WHERE Id_Encargado = :Id_Encargado";
            $q = $this->default->prepare($query);
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
    
    public function remove($data)
    {
        try 
        {
            $q = $this->default->prepare('UPDATE encargados SET Eliminado = :Eliminado WHERE Id_Encargado = :Id_Encargado');
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AlumnosModel extends Model
{
    public function __construct()
    {
        parent::__construct(array('default'));   
    }

    public function get_consulta()
    {
        $query = "SELECT 
                        alumnos.Id_Alumno,
                        alumnos.Nombre,
                        alumnos.Direccion ,
                        alumnos.Telefono ,
                        alumnos.Apellido,
                        alumnos.Vive_Con,
                        alumnos.FechaNacimiento,
                        religiones.Nombre AS Religion
                    FROM alumnos
                    INNER JOIN religiones ON alumnos.Id_Religion = religiones.Id_Religion
                    
                    WHERE alumnos.Eliminado = 0";
        $result = $this->default->prepare($query);
        $result->execute();
        return $result->fetchAll();
    }
    public function get_id($Id)
    {
        $params = array(":Id_Alumno" => $Id);
        $query = "SELECT * FROM alumnos WHERE Id_Alumno = :Id_Alumno AND Eliminado = 0";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetch();
    }
    public function save($data)
    {
        try 
        {
            $query = "INSERT INTO alumnos(Id_Religion,Nombre,Apellido,FechaNacimiento,Direccion,Telefono,Vive_Con) VALUES(:Id_Religion,:Nombre,:Apellido,:FechaNacimiento,:Direccion,:Telefono,:Vive_Con)";
            $q = $this->default->prepare($query);             
            $q->execute($data);
            return 0;
        } 
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }

    public function update($data)
    {
        try 
        {
            $query = "UPDATE alumnos SET  Id_Religion =:Id_Religion,Nombre =:Nombre,
            Apellido=:Apellido,FechaNacimiento=:FechaNacimiento,Direccion=:Direccion,Telefono=:Telefono,Vive_Con=:Vive_Con  WHERE Id_Alumno = :Id_Alumno";
            $q = $this->default->prepare($query);
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }

    
    
    public function remove($data)
    {
        try 
        {
            $q = $this->default->prepare('UPDATE alumnos SET Eliminado = :Eliminado WHERE Id_Alumno = :Id_Alumno');
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ResponsablesModel extends Model
{
    public function __construct()
    {
        parent::__construct(array('default'));   
    }

    public function get_consulta()
    {
        $query = "SELECT 
                        responsables.Id_Responsable,
                        responsables.Nombre,
                        responsables.Direccion,
                        responsables.Dui,
                        responsables.Telefono_Trabajo,
                        responsables.Apellido,
                        responsables.Lugar_trabajo,
                        responsables.Fecha_Nacimiento,
                        ocupaciones.Nombre AS Ocupacion
                    FROM responsables
                    INNER JOIN ocupaciones ON responsables.Id_Ocupacion = ocupaciones.Id_Ocupacion
                    
                    WHERE responsables.Eliminado = 0";
        $result = $this->default->prepare($query);
        $result->execute();
        return $result->fetchAll();
    }
    public function get_id($Id)
    {
        $params = array(":Id_Responsable" => $Id);
        $query = "SELECT * FROM responsables WHERE Id_Responsable = :Id_Responsable AND Eliminado = 0";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetch();
    }
    public function save($data)
    {
        try 
        {
            $query = "INSERT INTO responsables(Id_Ocupacion,Nombre,Apellido,Direccion,Telefono_Trabajo,Lugar_trabajo,Dui,Fecha_Nacimiento) VALUES(:Id_Ocupacion,:Nombre,:Apellido,:Direccion,:Telefono_Trabajo,:Lugar_trabajo,:Dui,:Fecha_Nacimiento)";
            $q = $this->default->prepare($query);             
            $q->execute($data);
            return 0;
        } 
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }

    public function update($data)
    {
        try 
        {
            $query = "UPDATE responsables SET  Id_Ocupacion =:Id_Ocupacion,Nombre=:Nombre,Apellido=:Apellido,Direccion=:Direccion,Telefono_Trabajo=:Telefono_Trabajo,Lugar_trabajo=:Lugar_trabajo,Dui=:Dui,Fecha_Nacimiento=:Fecha_Nacimiento  WHERE Id_Responsable = :Id_Responsable";
            $q = $this->default->prepare($query);
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }

    
    
    public function remove($data)
    {
        try 
        {
            $q = $this->default->prepare('UPDATE responsables SET Eliminado = :Eliminado WHERE Id_Responsable = :Id_Responsable');
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class UsuariosModel extends Model
{
    public function __construct()
    {
        parent::__construct(array('default'));   
    }

    public function get_consulta()
    {
        $query = "SELECT 
                        usuarios.Id_Usuario,
                        usuarios.User,
                        usuarios.Pasword AS Password,
                        roles.Nombre AS Rol,
                        estados.Nombre As Estado

                    FROM usuarios
                    INNER JOIN roles ON usuarios.Id_Rol = roles.Id_Rol
                    INNER JOIN estados ON usuarios.Id_Estado = estados.Id_Estado 
                    WHERE usuarios.Eliminado = 0";
        $result = $this->default->prepare($query);
        $result->execute();
        return $result->fetchAll();
    }
    public function get_id($Id)
    {
        $params = array(":Id_Usuario" => $Id);
        $query = "SELECT * FROM usuarios WHERE Id_Usuario = :Id_Usuario AND Eliminado = 0";
        $result = $this->default->prepare($query);
        $result->execute($params);
        return $result->fetch();
    }
    public function save($data)
    {
        try 
        {
            $query = "INSERT INTO usuarios(Id_Rol,Id_Estado,User,Pasword) VALUES(:Id_Rol,:Id_Estado,:User,:Password)";
            $q = $this->default->prepare($query);             
            $q->execute($data);
            return 0;
        } 
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }

    public function update($data)
    {
        try 
        {
            $query = "UPDATE usuarios SET  Id_Rol = :Id_Rol,  Id_Estado = :Id_Estado, User = :User WHERE Id_Usuario = :Id_Usuario";
            $q = $this->default->prepare($query);
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }

    public function update_password($data)
    {
        try 
        {
            $query = "UPDATE usuarios SET Pasword = :Password WHERE Id_Usuario = :Id_Usuario";
            $q = $this->default->prepare($query);
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
    
    public function remove($data)
    {
        try 
        {
            $q = $this->default->prepare('UPDATE usuarios SET Eliminado = :Eliminado WHERE Id_Usuario = :Id_Usuario');
            $q->execute($data);
            return 0;
        }
        catch (PDOException $e) 
        {
            echo 'Error '.$e->getMessage();
        }
        return 1;
    }
}
?>
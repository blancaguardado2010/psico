<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload 
{
	private $config;

	public function set_config($config = [])
	{
		$this->config = $config;
	}

	public function upload_images($new_name = "")
	{
	    $errors = array();
	    $response = [];
	    $temp = explode('.',$_FILES['images']['name']);
	    $images_ext = strtolower(end($temp));
		if(isset($_FILES['images']))
		{
		    $file_name = $_FILES['images']['name'];
		    $file_size = $_FILES['images']['size'];
		    $file_tmp = $_FILES['images']['tmp_name'];
		    $file_type = $_FILES['images']['type'];
		    
		    if(!in_array($images_ext, $this->config['expensions']))
		    {
		        $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
		    } 
		      
		    if($file_size > 2097152){
		        $errors[] = 'File size must be excately 2 MB';
		    }
		      
		    if(empty($errors) == true){
		        move_uploaded_file($file_tmp, $this->config['target_directory'] . ($new_name == "" ? $file_name : $new_name . "." . $images_ext));
		        $response['success'] = true;
		        $response['extension'] = $images_ext;
		        return $response;
		    }
		    $response['errors'] = $errors;
		    $response['has_errors'] = true;
		    return $response;
		}
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config["base_url"] = "http://localhost:81/fusalmo/";
$config['index_page'] = "";

# Database Configuration
$config['db'] = array(
    "default" => array(
        "dbname" => "fusalmo",
        "username" => "root",
        "password" => "",
        "port" => "3306",
        "host" => "localhost"
        )
);?>
